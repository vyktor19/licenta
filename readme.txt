So far:
- fisier package pentru instalare de dependinte (npm install) dupa instalare de node.js
- serverul creeaza conectiune intre 2 clienti (mai mult sau mai putin random)
- adaugat sistem de punctaj;
- jucatorii primesc o intrebare si 4 raspunsuri
- jucatorii primesc update la punctaje;
- facut formularul de intrebari (alpha stage);
- optiune de New Game;
- in caz ca oponentul se deconecteaza se notifica jucatorul si se afiseaza meniu principal
 - meniu principal
- afisarea raspunsului corect
- fisier de configurari
- separarea definitiei clasei de restul serverului
- trimiterea raspunsurilor in ordine random, dar ambii jucatori le primesc in aceeasi ordine

To do:
- sistem de partajare in caz de egalitate ?
- interfata mai atractiva


Install: install node.js
In site root se ruleaza in cmd instructiunea: npm install
Pentru pornirea aplicatiei: node server.js