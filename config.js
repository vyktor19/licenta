// Database host.
var host = "localhost";
// Database username.
var user = "root";
// Database password.
var pass = "";
// Database name.
var db_name = 'licenta';
// Database name.
var socketPath = '';
// Questions to be asked per game.
var questions_per_game = 2;
// Time allowed per question.
var time_per_question = 9;

exports.host = function () {
    return host;
};

exports.socket = function () {
    return socketPath;
};

exports.user = function () {
    return user;
};

exports.pass = function () {
    return pass;
};

exports.database = function () {
    return db_name;
};

exports.questions_per_game = function () {
    return questions_per_game;
};

exports.time_per_question = function () {
    return time_per_question;
};