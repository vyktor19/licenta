var Moniker = require('moniker');

/**
 * Class Player which holds the details of a player.
 *
 * @returns {Player}
 * @constructor
 */
var Player = function () {
    this.points = 0;
    this.name = "";
    this.opponent = false;
    this.questions = 0;
    this.sent = false;
    this.timeVal = 0;
    this.answer = '';

    this.addPoint = function () {
        this.points++;
        return this;
    };

    this.addQuestions = function () {
        this.questions++;
        return this;
    };

    this.resetQuestions = function () {
        this.questions = 0;
        return this;
    };

    this.getQuestions = function () {
        return this.questions;
    };

    this.resetPoints = function () {
        this.points = 0;
        return this;
    };

    this.setOpponent = function (op) {
        this.opponent = op;
        return this;
    };

    this.resetOpponent = function () {
        this.opponent = false;
        this.resetQuestions().resetPoints().unsetFlag();
        return this;
    };

    this.getOpponent = function () {
        return this.opponent;
    };

    this.getName = function () {
        return this.name;
    };

    this.setName = function (name) {
        if (!this.name) {
            if (name) {
                this.name = name;
            } else {
                this.name = Moniker.choose();
            }
        }
        return this;
    };

    this.getPoints = function () {
        return this.points;
    };

    this.setFlag = function () {
        this.sent = true;
        return this;
    };

    this.unsetFlag = function () {
        this.sent = false;
        return this;
    };

    this.getFlag = function () {
        return this.sent;
    };

    this.setTimerVal = function (val) {
        this.timeVal = val;
        return this;
    };

    this.unsetTimerVal = function () {
        clearInterval(this.timeVal);
        return this;
    };

    this.setAnswer = function (ans) {
        this.answer = ans;
        return this;
    };

    this.getAnswer = function () {
        return this.answer;
    };

    return this;
};

exports.Player = function () {
    return new Player;
};