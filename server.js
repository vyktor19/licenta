var app = require('http').createServer(handler),
    io = require('socket.io').listen(app, {log: false}),
    fs = require('fs'),
    mysql = require('mysql'),
    config = require('./config'),
    player = require('./player'),
    db = mysql.createConnection({
        host: config.host(),
        user: config.user(),
        password: config.pass(),
        database: config.database(),
        socketPath: config.socket()
    });

var max_questions = config.questions_per_game(),
    time = config.time_per_question();

// Log any errors connected to the db
db.connect(function (err) {
    if (err) console.log(err);
});

app.listen(8888);

/**
 * Array of Players
 *
 * @type {Array}
 */
var people = [];
// get the number of total questions.
var nr_questions = 0;
db.query('SELECT max(id) FROM intrebari;')
    .on('result', function (data) {
        nr_questions = data['max(id)'];
    });

function handler() {}

io.sockets.on('connection', function (client) {

    addPlayer(client.id);

    client.on('login', function (data) {
        people[client.id].setName(data['name']);
        client.emit('welcome', {you: people[client.id].name});
    });

    client.on('disconnect', function () {
        removePlayer(client.id);
    });

    client.on('answer', function (data) {
        if (data['answer'] == people[client.id].getAnswer()) {
            people[client.id].addPoint();
        }
        client.emit('correct', {answer: people[client.id].getAnswer()});
        people[client.id].unsetFlag();
        refreshPoints(client.id);
    });

    client.on('restart', function () {
        newGame(client.id);
    });
});

/**
 * Removes a client and announces the opponent that he has disconnected..
 *
 * @param id
 */
function removePlayer(id) {
    var op = people[id].getOpponent();
    people[id].unsetTimerVal();
    delete people[id];
    if (op && people[op]) {
        people[op].unsetTimerVal();
        if (people[op].getQuestions() < nr_questions || people[op].getFlag()) {
            io.sockets.socket(op).emit("un-coupled");
        }
    }
    coupleOpponents();
}

/**
 * The player has requested a New Game so unset his opponent and search for a new one.
 *
 * @param id
 */
function newGame(id) {
    var op = people[id].getOpponent();
    people[id].unsetTimerVal().resetOpponent();
    if (op && people[op]) {
        people[op].unsetTimerVal();
    }
    coupleOpponents();
}

/**
 * Adds client and tries to re-couple clients that do not have a connection.
 *
 * @param id
 */
function addPlayer(id) {
    people[id] = player.Player().setOpponent(1);
}

/**
 * Send to the player and his opponent the score.
 *
 * @param id
 */
function refreshPoints(id) {
    io.sockets.socket(id).emit("you-points", {points: people[id].getPoints()});
    var opponent = people[id].getOpponent();
    io.sockets.socket(opponent).emit("opponent-points", {points: people[id].getPoints()});
}

/**
 * Tries to couple players that do not have an opponent but want one.
 */
function coupleOpponents() {
    for (var key in people) {
        if (people.hasOwnProperty(key) && people[key].getOpponent() == false) {
            for (var key2 in people) {
                if (people.hasOwnProperty(key2) && (people[key2].getOpponent() == false && key2 != key)) {
                    people[key2].setOpponent(key).resetQuestions();
                    people[key].setOpponent(key2).resetQuestions();
                    io.sockets.socket(key).emit("coupled", {opponent: people[key2].getName()});
                    io.sockets.socket(key2).emit("coupled", {opponent: people[key].getName()});
                    sendQuestion(key);
                }
            }
        }
    }
}

function sendQuestion(id) {
    var player_1 = people[id];
    var player_2 = people[player_1.getOpponent()];
    if (people[id].getQuestions() < max_questions && (!player_1.getFlag() || !player_2.getFlag())) {
        player_1.addQuestions();
        player_2.addQuestions();
        var remaining = time;
        db.query('SELECT * FROM intrebari WHERE id >= ? limit 1;', Math.floor((Math.random() * nr_questions) + 1))
            .on('result', function (data) {
                var answers = [data['raspuns_1'], data['raspuns_2'], data['raspuns_3'], data['raspuns_4']];
                shuffle(answers);
                io.sockets.socket(id).emit("question", {
                    question: data['intrebare'],
                    answer_1: answers[0],
                    answer_2: answers[1],
                    answer_3: answers[2],
                    answer_4: answers[3],
                    nr_question: player_1.getQuestions(),
                    time: remaining
                });
                player_1.setFlag().setAnswer(data['raspuns_' + data['corect']]);
                io.sockets.socket(player_1.getOpponent()).emit("question", {
                    question: data['intrebare'],
                    answer_1: answers[0],
                    answer_2: answers[1],
                    answer_3: answers[2],
                    answer_4: answers[3],
                    nr_question: player_2.getQuestions(),
                    time: remaining
                });
                player_2.setFlag().setAnswer(data['raspuns_' + data['corect']]);
            });

        var intval = setInterval(function () {
            if (remaining > -1) {
                io.sockets.socket(id).emit("tick", {time: remaining});
                io.sockets.socket(player_1.getOpponent()).emit("tick", {time: remaining});
                remaining--;
            } else {
                requestAnswers(id);
            }
        }, 1000);

        player_1.setTimerVal(intval);
    } else {
        sendResults(id);
    }
}

/**
 * Request answer to the question from players.
 *
 * @param id
 */
function requestAnswers(id) {
    if (id && people[id]) {
        people[id].unsetTimerVal();
        people[people[id].getOpponent()].unsetTimerVal();

        io.sockets.socket(id).emit("answer");
        io.sockets.socket(people[id].getOpponent()).emit("answer");
        setTimeout(function () {
            sendQuestion(id);
        }, 1500);
    }
}

/**
 * Sends the final results of the game.
 *
 * @param id
 */
function sendResults(id) {
    var player_1 = people[id];
    var player_2 = people[people[id].getOpponent()];

    if (player_1.getFlag() || player_2.getFlag()) {
        setTimeout(function () {
            sendQuestion(id);
        }, 500);
    } else {
        var winner = getWinner(player_1, player_2);
        io.sockets.socket(id).emit("results", {winner: winner});
        io.sockets.socket(people[id].getOpponent()).emit("results", {winner: winner});
    }
}

/**
 * Decides the winner of the game.
 *
 * @param player_1
 * @param player_2
 * @returns {*}
 */
function getWinner(player_1, player_2) {
    if (player_1.getPoints() > player_2.getPoints()) {
        return player_1.getName();
    } else if (player_1.getPoints() < player_2.getPoints()) {
        return player_2.getName();
    } else {
        return player_1.getName() + " and " + player_2.getName();
    }
}

// Fisher-Yates (aka Knuth) shuffle implementation.
function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue,
        randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}