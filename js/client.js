var socket;
var username = "";

$(document).ready(function () {
    $(document).bind('keydown.key13', function (e) {
        if (e.which == 13) {
            $('form').submit();
        }
    });

    $('form').submit(function (event) {
        username = $("input#user").val();
        var password = $("input#password").val();
        $.ajax({
            url: "login.php",
            type: "POST",
            data: {
                user: username,
                password: password
            }
        }).fail(function (data){
            alert("Eroare la trimiterea formularului");
        }).done(function (data){
            if (data == 'success') {
                createWebSocket();
            } else if (data == 'fail') {
                alert("login fail");
            } else {
                alert("Conectare la server esuata");
            }
        });
        event.preventDefault();
    });
});

function createWebSocket() {
    $(document).unbind('keydown.key13');

    $("form").html("");
    $('#guestLogin').html("");

    socket = io.connect('http://localhost:8888', {resource: 'socket.io'});

    socket.emit('login', {name: username});

    socket.on('welcome', function (data) {
        reset();
        $('#you_are').html('Esti <b>' + data['you'] + '</b>');
        document.title = data['you'];
    });

    socket.on('coupled', function (data) {
        $('#alert').hide(200);
        $('#results').hide(200);
        $('#search').hide(200);
        $('#menu').hide(200);
        $('#opponent_is').html('Oponentul este <b>' + data['opponent'] + '</b>').show(200);
        $('#opponent_points').html('Oponentul are <b>0</b> puncte').show(200);
        $('#you_points').html('Ai <b>0</b> puncte').show(200);
    });

    socket.on('you-points', function (data) {
        $('#you_points').html('Aveti <b>' + data['points'] + '</b> puncte');
    });

    socket.on('opponent-points', function (data) {
        $('#opponent_points').html('Oponentul are <b>' + data['points'] + '</b> puncte');
    });

    socket.on('question', function (data) {
        $('#question').text(data['nr_question'] + '. ' + data['question']);
        $('#answer_1').children('span').text(data['answer_1']);
        $('#answer_2').children('span').text(data['answer_2']);
        $('#answer_3').children('span').text(data['answer_3']);
        $('#answer_4').children('span').text(data['answer_4']);
        updateTimer(data['time']).show(200);
        displayForm();
        unclickAll();
    });

    socket.on('answer', function () {
        sendAnswer();
    });

    socket.on('results', function (data) {
        $('#question').hide(200);
        $('#timer').hide(200);
        $('#answers').hide(200);
        $('#results').html('<span>Winner:\n' + data['winner'] + '</span>').show('');
        $('#menu').show(200);
    });

    socket.on('tick', function (data) {
        updateTimer(data['time']);
    });

    socket.on('un-coupled', function () {
        reset();
        $('#alert').show(200);
        $('#search').hide(200);
    });

    socket.on('correct', function (data) {
        $('#answer').addClass('blocked');
        var right_answer = $("div > span:contains('" + data['answer'] + "')").parent();
        var selected_answer = $('.clicked');
        if (right_answer.text() != selected_answer.text()) {
            selected_answer.addClass('wrong');
        }
        selected_answer.removeClass('clicked');
        right_answer.addClass('correct');
    })
}

function reset() {
    $('#alert').hide(200);
    $('#you_points').hide(200);
    $('#opponent_is').hide(200);
    $('#opponent_points').hide(200);
    $('#timer').hide(200);
    $('#results').hide(200);
    $('#answers').hide(200);
    $('#question').hide(200);
    $('#menu').show(200);
    $('#search').hide(200);
    $('#instructions').hide(200);
}

function unclickAll() {
    $('.clicked').removeClass('clicked');
    $('.blocked').removeClass('blocked');
    $('.correct').removeClass('correct');
    $('.wrong').removeClass('wrong');
}

function answerClicked(div) {
    if (!$('.clicked').html()) {
        $(div).addClass('clicked');
    }
}

function displayForm() {
    $('#question').show(200);
    $('#answers').show(200);
}

function updateTimer(timer) {
    return $('#timer').text(timer);
}

function sendAnswer() {
    var ans = '';
    if ($('.clicked').html()) {
        ans = $('.clicked > span').text();
    }
    socket.emit('answer', {answer: ans});
}

function newGame() {
    socket.emit('restart');
    reset();
    $('#menu').hide(200);
    $('#search').show(200);
}

function showInstructions() {
    $('#instructions_menu').hide(200);
    $('#instructions').show(200);
}